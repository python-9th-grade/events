#!/bin/sh

set -ex

python3 --version
python3 -m venv venv
source venv/bin/activate
python --version
pip install -r requirements.txt
pip list
