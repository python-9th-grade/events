import tkinter
from PIL import ImageTk, Image

root = tkinter.Tk()
root.title('Птахи')
root.geometry('275x195')

lb1=tkinter.Label(root, text='', font='Arial 16', width=25, bg='grey', fg='white')
lb1.place(x=20, y=150)

image1=Image.open('kolibri.gif')
image1=image1.resize((100, 100), Image.ANTIALIAS)
image1=ImageTk.PhotoImage(image1)
# image1=tkinter.PhotoImage(file='kolibri.gif')

def btn1_click():
    lb1.config(text='Колібрі')

btn1=tkinter.Button(root, image=image1, command=btn1_click)
btn1.place(x=20, y=20)

image2=Image.open('orel.jpeg')
image2=image2.resize((100, 100), Image.ANTIALIAS)
image2=ImageTk.PhotoImage(image2)
# image2=tkinter.PhotoImage(file='orel.jpeg')

def btn2_click():
    lb1.config(text='Орел')

btn2=tkinter.Button(root, image=image2, command=btn2_click)
btn2.place(x=140, y=20)

root.mainloop()
