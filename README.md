# Інформатика

## Домашнє завдання за 08.04.2021

1. Перегляньте відео:
    - Частина 1. www.youtube.com/watch?v=sfU7XU5PFfY
    - Частина 2. www.youtube.com/watch?v=rvkW4tuG60k
2. Виконайте практичне завдання:
    - cutt.ly/9cHcSCR

## Рішення

У роботі були використані такі зображення: [kolibri.gif](https://99px.ru/sstorage/86/2017/07/image_862207171327506550896.gif) та [orel.jpeg](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpPDWaYMqvpXJ0ePCQtZsfliWWkrqRiKQChhdxZEyWF5W63pqE2qti9BeCjr_3_Ovydbc).

Для маштабування зображення була використана бібліотека [Pillow](https://pypi.org/project/Pillow/) та [Tutorial #77](https://youtu.be/w0mtuHsE7IM).

## Перевірка

Для налаштування середовища необхідно виконати `./install.sh` з корневої папки.

Для запуску додатка необхідно виконати `./run.sh` з корневої папки.
